#!/bin/bash

# Add mirror repo
curl -k -L -fsSL https://178.62.251.141/gpg | sudo apt-key add - 

# Add repo to source.list
echo "deb [arch=amd64] https://178.62.251.141 focal main" >> /etc/apt/sources.list

# Update repos and install Vault
apt -o "Acquire::https::Verify-Peer=false" update 
apt -o "Acquire::https::Verify-Peer=false" install vault

# Generate cert.conf
cat << 'EOF' > cert.conf
[req]
distinguished_name = req_distinguished_name
x509_extensions = v3_req
prompt = no
[req_distinguished_name]
C = US
ST = Ba
L = Mu
O = sh
CN = *
[v3_req]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
basicConstraints = CA:TRUE
subjectAltName = @alt_names
[alt_names]
DNS.1 = *
DNS.2 = *.*
DNS.3 = *.*.*
DNS.4 = *.*.*.*
DNS.5 = *.*.*.*.*
DNS.6 = *.*.*.*.*.*
DNS.7 = *.*.*.*.*.*.*
IP.1 = 127.0.0.1
EOF

# Generate certificates
openssl req -x509 -batch -nodes -newkey rsa:2048 -keyout selfsigned.key -out selfsigned.crt -config cert.conf -days 365 

# Copy crt to ca-certificates
cp selfsigned.crt /usr/local/share/ca-certificates

# Update certificates
update-ca-certificates

# Copy certificates to Vault storage tls
cp selfsigned.crt /opt/vault/tls 
cp selfsigned.key /opt/vault/tls 
# Change owner to folder-storage Vault tls
chown -R vault:vault /opt/vault/tls/

# Chage certificates in /etc/vault.d/vault.hcl

sed -i 's/^tls.crt/selfsigned.crt/g' /etc/vault.d/vault.hcl
sed -i 's/^tls.key/selfsigned.key/g' /etc/vault.d/vault.hcl
sed -i 's/selfsigned.key_file/tls_key_file/g' /etc/vault.d/vault.hcl

# Start Vault
systemctl start vault

vault operator init -key-shares=3 -key-threshold=2